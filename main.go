package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"time"
)

func main() {

	rand.Seed(time.Now().UnixNano())

	var gsj GameSessionJSON
	err := gsj.Load("./samples/gs_before_dealing_sample.json")
	if err != nil {
		log.Panicf("error loading GameSessionJSON: %s", err)
	}
	log.Printf("Raw gsj:\n%v\n", gsj)

	gs, err := gsj.Convert2Math()
	if err != nil {
		log.Printf("error converting JSON to math: %s", err)
	}
	log.Printf("Total %d cards in deck", len(gs.Deck))

	gsJSON, err := json.Marshal(gs)
	if err != nil {
		log.Printf("error while converting loaded GameStructure to JSON: %s", err)
	}
	log.Printf("Loaded GameSession structure:\n %s\n", string(gsJSON))

	consistent, reason := gs.Deck.CheckDeckConsistency()
	if !consistent {
		log.Printf("Deck is inconsistent, reason: %s", reason)
	} else {
		log.Printf("Deck is consistent")
	}

	gs.AutoDeal()
	log.Printf("After dealing:\n%s\n", gs.Visualize())
	log.Printf("Deal consistent: %t\n", len(gs.Players[0].Stack)+len(gs.Players[1].Stack)+len(gs.Players[2].Stack) == 52)

}
