This is a ["Po plokhoy"](http://www.wapbox.ru/my.php?go=list_blog&whob=53270) multiplayer card game backend implementation

# Basic concept

There are N players in the game, 2 ≥ *N* ≥ x, x is unknown, need to check up.

N used to be at least 3 IRL, no joy playing game with only two players.

GameSessionJSON.Deck is a randomized standard 52-card stack of cards.

GameSessionJSON.Deck is an input for *dealing*.

*N* GameSessionJSON.Player.Stacks are outputs of *dealing*.

Rotations (passing the move, checking if own top card is higher than other players' top cards) happening clockwise

## Card ranks

- While *dealing*:

... 2 > A > K > Q > J > 10 > 9 > 8 > 7 > 6 > 5 > 4 > 3 > 2 > A ...

- In the *game* itself:

A > K > Q > J > 10 > 9 > 8 > 7 > 6 > 5 > 4 > 3 > 2

## Suits, trump

During *dealing* suits don't matter.

In the *game* round:

- Spades ♠️ playing their own game

No other suits able to win spades. Spades unable win over other suits. Spades cannot be trump

A♠️ > K♠️ > ... > 2♠️

- Other suits act normal

A♣️ > K♣️ > ... > 2♣️
A♦️ > K♦️ > ... > 2♦️
A♥️ > K♥️ > ... > 2♥️

- Trump suit defined as the first non-spades suit in GameSessionJSON.Deck

It means that player, who got last non-spades card have to show this card to all players and declare trump suit

- Trump suit wins over other suits, except spades

If trumps are clubs:

... > 3♣️ > 2♣️ > A♦️ > ...

... > 3♣️ > 2♣️ > A♥️ > ...

but remember about spades - they are playing their own game

## Dealing

Random player starts *dealing* by pulling top Deck card, putting it front up to his own Stack and passing move to next hand.

Next player should first check his own Stack's top card (empty for the first round) for winning other players' top cards. If so, he puts this card front up to suitable player's Stack and checks next top card of his own Stack. 

Otherwise, he pulls top Deck card, checks if it wins over other players top cards, if no - puts onto his own Stack and pass the move.

Next players doing same things. *Dealing* continues till last card from Deck

## AutoDeal

*AutoDeal* converts GameSessionJSON.Deck to *N* GameSessionJSON.Player.Stacks definitely


## First Player

Player next to trump opener starts the game

## Game
