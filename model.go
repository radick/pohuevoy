package main

import (
	"github.com/google/uuid"
)

// Math model

// Players slice is n-element finite field when determining next one to move

// Cards are n-element field by rank values which is finite while g.Dealing
// 10♧ covers 9❤ because 10 is higher than 9
// 2♦ covers A♠ because 2 is higher than A
// in game's hierarchy vicious circle:
// ..., 2 > A > K > Q > J > 10 > 9 > 8 > 7 > 6 > 5 > 4 > 3 > 2 > A ...

// While g.Playing ranks works as usual hierarchy:
// A > K > Q > J > 10 > 9 > 8 > 7 > 6 > 5 > 4 > 3 > 2

// mappingSuitVisual maps suit number to its visualization
var mappingSuitVisual = map[int]string{
	0: "♣️",
	1: "♦️",
	2: "❤️",
	3: "♠️",
}

// mappingRankVisual maps rank to its visualization
var mappingRankVisual = map[int]string{
	-1: "empty",
	0:  "2",
	1:  "3",
	2:  "4",
	3:  "5",
	4:  "6",
	5:  "7",
	6:  "8",
	7:  "9",
	8:  "10",
	9:  "J",
	10: "Q",
	11: "K",
	12: "A",
}

// mappingSuitJSON maps suit to its json representation
var mappingSuitJSON = map[int]string{
	-1: "empty",
	0:  "clubs",
	1:  "diamonds",
	2:  "hearts",
	3:  "spades",
}

// reverseMappingSuit maps suit to its number
var reverseMappingSuit = map[string]int{
	"empty":    -1,
	"clubs":    0,
	"diamonds": 1,
	"hearts":   2,
	"spades":   3,
}

// reverseMappingRank maps rank to its number
var reverseMappingRank = map[string]int{
	"empty": -1,
	"2":     0,
	"3":     1,
	"4":     2,
	"5":     3,
	"6":     4,
	"7":     5,
	"8":     6,
	"9":     7,
	"10":    8,
	"J":     9,
	"Q":     10,
	"K":     11,
	"A":     12,
}

// Card represents card
// 1st index is suit: 0 - clubs (♣), 1 - diamonds (♦), 2 - hearts (♥) and 3 - spades (♠)
// 2nd index is rank:
// 0 - "2",
// 1 - "3",
// 2 - "4",
// 3 - "5",
// 4 - "6",
// 5 - "7",
// 6 - "8",
// 7 - "9",
// 8 - "10",
// 9 - "J",
// 10 - "Q",
// 11 - "K",
// 12 - "A"
// {-1, -1} is "error code" (unused for now)
type Card [2]int

// CardJSON contains Card in json format
type CardJSON struct {
	Suit string `json:"suit"`
	Rank string `json:"rank"`
}

// Stack is a pile of Card
// Most stacks are LIFO while alteration
type Stack []Card

// ReadTopCard reads stack top card
func (s *Stack) ReadTopCard() (topCard Card) {
	if len(*s) > 0 {
		return (*s)[len(*s)-1]
	}
	return topCard
}

// StackJSON is a pile of CardJSON
type StackJSON []CardJSON

// gameStatus maps default game status codes to their string descriptions
// Draft for future needs
var gameNetworkStatus = map[int]string{
	0: "not started",
	1: "not started - waiting for players",
	2: "started - ongoing, all players online",
	3: "started - paused, waiting for player(s)",
	4: "started - ongoing, some players replaced with bot",
	5: "stopped - ok",
	6: "stopped - error",
}

// reverseGameNetworkStatus maps string network statuses to its codes
// Draft for future needs
var reverseGameNetworkStatus = map[string]int{
	"not started":                                       0,
	"not started - waiting for players":                 1,
	"started - ongoing, all players online":             2,
	"started - paused, waiting for player(s)":           3,
	"started - ongoing, some players replaced with bot": 4,
	"stopped - ok":                                      5,
	"stopped - error":                                   6,
}

// Player represents player in game
// Players slice is n-element finite field
type Player struct {
	ID    uuid.UUID
	Name  string
	Stack Stack
}

// Create Player with given name
func (p *Player) Create(name string) {
	p.ID = uuid.New()
	p.Name = name
	p.Stack = Stack{}
}

// PlayerJSON contains Player in json format
type PlayerJSON struct {
	ID    string    `json:"ID"`
	Name  string    `json:"Name"`
	Stack StackJSON `json:"Stack"`
}

// GameSession represents game session
type GameSession struct {
	ID               uuid.UUID
	Deck             Stack
	NetworkStatus    int
	Dealing          bool
	Playing          bool
	TrumpSuit        int // matches suits, but not spades, hence 0..2
	Dealer           int
	NextPlayer       int
	PrevPlayer       int
	CardOverTable    Card
	Players          []Player
	RankMatchPlayers []Player
}

// Init creates GameSession with given player names
func (g *GameSession) Init(playerNames []string) {
	g.ID = uuid.New()
	g.CardOverTable = Card{-1, -1}
	g.TrumpSuit = 3 // default value of trump is spades, will be changed next step
	g.GetRandomStandard52Deck()
	//g.Stack = Stack{}
	g.NetworkStatus = 0
	g.Dealer = 0
	g.NextPlayer = (g.Dealer + 1) % len(playerNames)
	g.PrevPlayer = (g.Dealer - 1) % len(playerNames)
	for i := 0; i < len(playerNames); i++ {
		var p = Player{}
		p.Create(playerNames[i])
		g.Players = append(g.Players, p)
	}
}

// NewGameSession returns new GameSession
func NewGameSession() *GameSession {
	return &GameSession{
		ID:               uuid.New(),
		Deck:             Stack{},
		NetworkStatus:    0,
		Dealing:          false,
		Playing:          false,
		TrumpSuit:        0,
		Dealer:           0,
		NextPlayer:       0,
		PrevPlayer:       0,
		CardOverTable:    Card{},
		Players:          nil,
		RankMatchPlayers: nil,
	}
}

// GameSessionJSON contains GameSession data in json format
type GameSessionJSON struct {
	ID               string       `json:"ID"`
	Deck             StackJSON    `json:"Deck"`
	NetworkStatus    string       `json:"NetworkStatus"`
	Dealing          bool         `json:"Dealing"`
	Playing          bool         `json:"Playing"`
	TrumpSuit        string       `json:"TrumpSuit"`
	ActivePlayer     int          `json:"Dealer"`
	CardOverTable    CardJSON     `json:"CardOverTable"`
	Players          []PlayerJSON `json:"Players"`
	RankMatchPlayers []PlayerJSON `json:"RankMatchPlayers"`
}
