package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"io"
	"io/ioutil"
	"os"
)

// Load loads GameSessionJSON from file named fileName
func (gsj *GameSessionJSON) Load(fileName string) error {
	jsonFile, err := os.Open(fileName)
	if err != nil {
		return err
	}
	//defer jsonFile.Close()

	bytes, err := io.ReadAll(jsonFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(bytes, &gsj)
	if err != nil {
		return err
	}

	return nil
}

// Save saves GameSessionJSON to file names fileName
func (gsj *GameSessionJSON) Save(fileName string) error {
	if _, err := os.Stat(fileName); err == nil {
		return err
	}

	jsonFile, err := json.MarshalIndent(&gsj, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(fileName, jsonFile, 0644)
	if err != nil {
		return err
	}
	return nil
}

// Visualize Card as a unicode string
func (c Card) Visualize() (visual string) {
	return mappingRankVisual[c[1]] + mappingSuitVisual[c[0]]
}

// Visualize Stack from bottom to top as a unicode string
func (s *Stack) Visualize() (visual string) {
	for _, card := range *s {
		visual = visual + card.Visualize() + " "
	}
	return visual
}

// Visualize Player as a unicode string
func (p *Player) Visualize() (visual string) {
	// ToDo Wha?? 🤷
	return p.Name + ": " + p.Visualize()
}

// Convert2JSON converts Player to PlayerJSON
func (p *Player) Convert2JSON() (playerJSON PlayerJSON) {
	playerJSON.ID = p.ID.String()
	playerJSON.Name = p.Name
	playerJSON.Stack = p.Stack.Convert2JSON()
	return playerJSON
}

// Visualize GameSession as a unicode string
func (g *GameSession) Visualize() (visual string) {
	var visualPlayers string
	for i, player := range g.Players {
		isActive := " "
		if i == g.Dealer {
			isActive = "*"
		}
		visualPlayers = visualPlayers + fmt.Sprintf("\t%s%d(%d): %s\n", isActive, i, len(player.Stack), player.Stack.Visualize())
	}
	return fmt.Sprintf("UUID: %s\n\tGame deck(%d): %s\n\tPlayers:\n%s", g.ID, len(g.Deck), g.Deck.Visualize(), visualPlayers)
}

// Convert2Math converts GameSessionJSON to its math model
func (gsj *GameSessionJSON) Convert2Math() (gs GameSession, err error) {
	gs.ID, err = uuid.Parse(gsj.ID)
	if err != nil {
		return GameSession{}, err
	}

	fmt.Printf("gs.ID: %s\n", gs.ID)

	for _, cardJSON := range gsj.Deck {
		card, err := cardJSON.Convert2Math()
		if err != nil {
			return GameSession{}, errors.New("unable to convert card")
		}
		gs.Deck.PushCard(card)
	}

	var ok bool
	gs.NetworkStatus, ok = reverseGameNetworkStatus[gsj.NetworkStatus]
	if !ok {
		return GameSession{}, err
	}

	gs.Dealing = gsj.Dealing
	gs.Playing = gsj.Playing

	gs.TrumpSuit, ok = reverseMappingSuit[gsj.TrumpSuit]
	if !ok {
		return GameSession{}, errors.New("trump detection failed")
	}
	gs.Dealer = gsj.ActivePlayer
	gs.CardOverTable, err = gsj.CardOverTable.Convert2Math()
	if err != nil {
		return GameSession{}, err
	}

	for _, playerJSON := range gsj.Players {
		player, err := playerJSON.Convert2Math()
		if err != nil {
			return GameSession{}, err
		}
		gs.Players = append(gs.Players, player)
	}

	for _, playerJSON := range gsj.RankMatchPlayers {
		player, err := playerJSON.Convert2Math()
		if err != nil {
			return GameSession{}, err
		}
		gs.RankMatchPlayers = append(gs.RankMatchPlayers, player)
	}

	return gs, nil
}

// Convert2JSON converts GameSession to its JSON model
func (g *GameSession) Convert2JSON() (gsj GameSessionJSON) {
	gsj.ID = g.ID.String()
	gsj.Deck = g.Deck.Convert2JSON()
	gsj.NetworkStatus = gameNetworkStatus[g.NetworkStatus]
	gsj.Dealing = g.Dealing
	gsj.Playing = g.Playing
	gsj.TrumpSuit = mappingSuitJSON[g.TrumpSuit]
	gsj.ActivePlayer = g.Dealer
	gsj.CardOverTable = g.CardOverTable.Convert2JSON()

	for _, player := range g.Players {
		gsj.Players = append(gsj.Players, player.Convert2JSON())
	}

	for _, player := range g.RankMatchPlayers {
		gsj.RankMatchPlayers = append(gsj.RankMatchPlayers, player.Convert2JSON())
	}

	return gsj
}
