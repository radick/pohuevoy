package main

import (
	"fmt"
	"github.com/google/uuid"
	"log"
	"math/rand"
)

// GetStandard52Deck returns all 52 Card in standard 52-deck as a Stack
func GetStandard52Deck() (full52Deck Stack) {
	for suit := 0; suit < 4; suit++ {
		for rank := 0; rank < 13; rank++ {
			full52Deck = append(full52Deck, Card{suit, rank})
		}
	}
	return full52Deck
}

// GetRandomStandard52Deck returns random Stack of 52 Card
// Maybe it is a good idea to calc trump card here. Maybe not
func (g *GameSession) GetRandomStandard52Deck() {
	full52Deck := GetStandard52Deck()
	for len(full52Deck) > 0 {
		randomNumber := rand.Intn(len(full52Deck))
		g.Deck = append(g.Deck, full52Deck[randomNumber])
		full52Deck = append(full52Deck[:randomNumber], full52Deck[randomNumber+1:]...)
	}

	// Check for trump card
	for g.TrumpSuit == 3 {
		for i := len(g.Deck) - 1; i >= 0; i-- {
			if g.Deck[i][0] != 3 {
				g.TrumpSuit = g.Deck[i][0]
				break
			}
		}
	}
}

// Convert2JSON converts Card to CardJSON
func (c Card) Convert2JSON() (cardJSON CardJSON) {
	cardJSON.Suit = mappingSuitJSON[c[0]]
	cardJSON.Rank = mappingRankVisual[c[1]]
	return cardJSON
}

// Convert2Math converts CardJSON to Card
func (c *CardJSON) Convert2Math() (card Card, err error) {
	card[0] = reverseMappingSuit[c.Suit]
	card[1] = reverseMappingRank[c.Rank]
	return card, nil
}

// IsEmpty returns true if Stack is empty
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

// PullCard pulls top card out of stack
func (s *Stack) PullCard() (topCard Card, isEmpty bool) {
	if s.IsEmpty() {
		return Card{}, true
	}
	indexOfLastOne := len(*s) - 1
	if indexOfLastOne >= 0 {
		topCard := Card{
			(*s)[indexOfLastOne][0],
			(*s)[indexOfLastOne][1],
		}
		*s = (*s)[:indexOfLastOne]
		return topCard, false
	}
	return Card{}, true
}

// PushCard puts given card on top of stack
func (s *Stack) PushCard(card Card) {
	*s = append(*s, card)
}

// Convert2JSON converts Stack to StackJSON
func (s *Stack) Convert2JSON() (stackJSON StackJSON) {
	for _, card := range *s {
		cardJSON := card.Convert2JSON()
		stackJSON = append(stackJSON, cardJSON)
	}
	return stackJSON
}

// Convert2Math converts PlayerJSON to Player
func (pj *PlayerJSON) Convert2Math() (player Player, err error) {
	player.ID, err = uuid.Parse(pj.ID)
	if err != nil {
		return Player{}, err
	}

	player.Name = pj.Name

	for _, cardJSON := range pj.Stack {
		card, err := cardJSON.Convert2Math()
		if err != nil {
			return Player{}, err
		}
		player.Stack.PushCard(card)
	}

	return player, nil
}

// CheckDeckConsistency checks Deck stack's integrity
// Idea is to sieve stack and compare it to GetStandard52Deck
func (s *Stack) CheckDeckConsistency() (consistent bool, reason string) {
	if len(*s) != 4*13 {
		return false, fmt.Sprintf("length of deck is %d != 4 * 13", len(*s))
	}

	// Init sieve deck (prefilled with -1)
	var sieveDeck [4][13]int
	for i := 0; i < 4; i++ {
		for j := 0; j < 13; j++ {
			sieveDeck[i][j] = -1
		}
	}

	for i := 0; i < len(*s); i++ {
		if sieveDeck[(*s)[i][0]][(*s)[i][1]] != -1 {
			return false, fmt.Sprintf("card %s duplicated", (*s)[i].Visualize())
		}
		sieveDeck[(*s)[i][0]][(*s)[i][1]] = 1 // Mark as present
	}

	return true, ""
}

// PassTheMove passes the move to next Player and calculates NextPlayer and PrevPlayer
func (g *GameSession) PassTheMove() {
	g.PrevPlayer = g.Dealer
	g.Dealer = (g.Dealer + 1) % len(g.Players)
	g.NextPlayer = (g.Dealer + 1) % len(g.Players)
}

// isHigher returns true if card1 rank is higher than card2 rank
func isHigher(card1, card2 Card) bool {
	if (card2[1]+1)%13 == card1[1] {
		return true
	}
	return false
}

// GetMatchPlayerNumber returns index of player, who's top card matches CardOverTable or -1 if there is no match
func (g *GameSession) GetMatchPlayerNumber() (matchPlayerNumber int) {
	for i := 1; i <= len(g.Players); i++ {
		if len(g.Players[i%len(g.Players)].Stack) > 0 && isHigher(g.CardOverTable, g.Players[i%len(g.Players)].Stack.ReadTopCard()) {
			matchPlayerNumber = i % len(g.Players)
			return matchPlayerNumber
		}
	}

	return -1
}

// AutoDeal automatically deal Deck for players
func (g *GameSession) AutoDeal() {
	g.Dealing = true
	var empty bool
	var steps = 0

	// Need to put first card from game deck to Dealer's stack and pass the move
	g.CardOverTable, empty = g.Deck.PullCard()
	log.Printf("\nCard %d pulled from deck: %s, stack len: %d, Active Player: %s", steps, g.CardOverTable.Visualize(), len(g.Players[g.Dealer].Stack), g.Players[g.Dealer].Name)
	if empty {
		return
	}
	g.Players[g.Dealer].Stack.PushCard(g.CardOverTable)
	g.PassTheMove()

	for len(g.Deck) > 0 {
		steps++
		log.Printf("\nIteration: %d", steps)
		// Dealer must check his own stack before pulling from Deck
		for !g.Players[g.Dealer].Stack.IsEmpty() {
			g.CardOverTable, _ = g.Players[g.Dealer].Stack.PullCard()
			log.Printf("\nCard %s pulled from OWN stack, stack len: %d, Active Player: %s", g.CardOverTable.Visualize(), len(g.Players[g.Dealer].Stack), g.Players[g.Dealer].Name)
			log.Printf("Own stack: %v", g.Players[g.Dealer].Stack.Visualize())

			playerNumber2Push := g.GetMatchPlayerNumber()
			if playerNumber2Push == -1 {
				log.Printf("\nNo match, putting %s back to OWN stack", g.CardOverTable.Visualize())
				g.Players[g.Dealer].Stack.PushCard(g.CardOverTable)
				break
			}

			if playerNumber2Push == g.Dealer {
				log.Printf("\nCard %s match only OWN stack's top card, putting it back", g.CardOverTable.Visualize())
				g.Players[g.Dealer].Stack.PushCard(g.CardOverTable)
				break
			}

			g.Players[playerNumber2Push].Stack.PushCard(g.CardOverTable)
			log.Printf("\nCard %s match with %s's top card", g.CardOverTable.Visualize(), g.Players[playerNumber2Push].Name)

		}

		// Now dealer can pull card from deck
		g.CardOverTable, empty = g.Deck.PullCard()
		if empty {
			break
		}
		log.Printf("\nCard %d pulled from Deck: %s, Active Player: %s, Prev Player: %s, Next Player: %s", steps, g.CardOverTable.Visualize(), g.Players[g.Dealer].Name, g.Players[g.PrevPlayer].Name, g.Players[g.NextPlayer].Name)

		playerNumber2Push := g.GetMatchPlayerNumber()
		if playerNumber2Push == -1 {
			g.Players[g.Dealer].Stack.PushCard(g.CardOverTable)
			log.Printf("It doesn't match with any, putting to OWN stack and passing the move")
			g.PassTheMove()
			continue
		}

		log.Printf("Match with %s who's top card is: %s", g.Players[playerNumber2Push].Name, g.Players[playerNumber2Push].Stack.ReadTopCard().Visualize())
		g.Players[playerNumber2Push].Stack.PushCard(g.CardOverTable)
	}

	g.Dealing = false
}
